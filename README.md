This automatically generates database model files for Android using the data structure of a specified table in mysql / PHPMyAdmin.

To use, just create a database in PHPMyAdmin (table names in plural form please), 
Open
src/mysqlmodelgenerator/MysqlModelGenerator.java
and customize the following variables:

    static String database="fengshuicalendar";
    static String hostname="localhost";
    static String username="root";
    static String password="password";

Then run the program. The generated files will appear in the folder 

/src/models

Copy them and paste to your Android project under the package "model". Also copy the contents of the folder src/utils and put them into your Android Project under the package "utils"