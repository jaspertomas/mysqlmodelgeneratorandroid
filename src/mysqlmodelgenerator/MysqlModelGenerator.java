/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mysqlmodelgenerator;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import utils.MySqlDBHelper;
import utils.fileaccess.FileWriter;

/**
 *
 * @author jaspertomas
 */
public class MysqlModelGenerator {
    
    static String database="docu";
//    static String database="pda";
//    static String database="myfreegas2";
    static String hostname="localhost";
    static String username="root";
    static String password="password";

    public static void main(String args[])
    {
        String url = "jdbc:mysql://"+hostname+":3306/"+database;

        MySqlDBHelper.init(url, username, password);            
        Connection conn=MySqlDBHelper.getInstance().getConnection();
        
        Statement st = null;
        ResultSet rs = null;
        ArrayList<String> tables=new ArrayList<String>();
        ArrayList<String> fields=new ArrayList<String>();
        ArrayList<String> fieldtypes=new ArrayList<String>();
        try { 
            st = conn.createStatement();
            rs = st.executeQuery("Show Tables");

            while (rs.next()) {
                tables.add(rs.getString(1));
            }
            for(String table:tables) {
                rs = st.executeQuery("SHOW COLUMNS FROM "+table);
                fields.clear();
                fieldtypes.clear();
                while (rs.next()) {
                    fields.add(rs.getString(1));
                    fieldtypes.add(rs.getString(2));
                }
                createBaseModelFile(table,fields,fieldtypes);
                createModelFile(table,fields,fieldtypes);
                createBaseModelsFile(table,fields,fieldtypes);
                createModelsFile(table,fields,fieldtypes);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public static void createModelsFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        //write file to current directory
        File outputdir=new File("./src/models");
        outputdir.mkdir();
        FileWriter.write(outputdir.getPath()+"/"+toCamelCase(table)+".java", createModelsFileContents(table,fields,fieldtypes));
    }
    public static String createModelFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        String output="package models;\n" +
"\n" +
"import android.database.Cursor;\n" +
"import models.base.Base[tableCaps];" +
"\n" +
"public class [tableCaps] extends Base[tableCaps]{\n" +
"	public [tableCaps]()\n" +
"	{\n" +
"	}\n"
+ "	public [tableCaps](Cursor cursor)\n" +
"	{\n" +
"		super(cursor);\n" +
"	}\n" +
"}";
        String tableplural=table;
        String tablesingular=singularize(table);
        String tablecapssingular=toCamelCase(tablesingular);
        String tablecapsplural=toCamelCase(tableplural);
        output=output.replace("[tableCaps]", tablecapssingular);
        output=output.replace("[table]", table);
        output=output.replace("[tableCapsPlural]", tablecapsplural);
        output=output.replace("[tablePlural]", tableplural);

        return output;
    }
    public static String createModelsFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        String output="package models;\n" +
"\n" +
"import java.util.ArrayList;\n" +
"\n" +
"import models.base.Base[tableCapsPlural];" +
"import utils.MyDatabaseHelper;\n" +
"import android.database.Cursor;\n" +
"import android.database.sqlite.SQLiteDatabase;" +
"\n" +
"public class [tableCapsPlural] extends Base[tableCapsPlural]{\n" +
"/*\n" +
"	public static void selectIdsAndNames(String criteria,		\n" +
"			ArrayList<Integer> itemIds,\n" +
"			ArrayList<String> itemNames\n" +
"			) {\n" +
"		itemNames.clear();\n" +
"		itemIds.clear();\n" +
"		SQLiteDatabase db = MyDatabaseHelper.getInstance()\n" +
"				.getWritableDatabase();\n" +
"\n" +
"		Cursor cursor = db.rawQuery(\"SELECT id, name FROM \"+tablename+\" \"+criteria, null);\n" +
"		while (cursor.moveToNext()) {\n" +
"			itemIds.add(cursor.getInt(cursor.getColumnIndex(\"id\")));\n" +
"			itemNames.add(cursor.getString(cursor.getColumnIndex(\"name\")));\n" +
"		}\n" +
"		cursor.close();\n" +
"		db.close();\n" +
"	}\n" + 
"*/\n" +
"}";
        String tableplural=table;
        String tablesingular=singularize(table);
        String tablecapssingular=toCamelCase(tablesingular);
        String tablecapsplural=toCamelCase(tableplural);
        output=output.replace("[tableCaps]", tablecapssingular);
        output=output.replace("[table]", table);
        output=output.replace("[tableCapsPlural]", tablecapsplural);
        output=output.replace("[tablePlural]", tableplural);

        return output;
    }
    public static void createBaseModelsFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        //write file to current directory
        File outputdir=new File("./src/models/base");
        outputdir.mkdir();
        FileWriter.write(outputdir.getPath()+"/Base"+toCamelCase(table)+".java", createBaseModelsFileContents(table,fields,fieldtypes));
    }
    public static String createBaseModelsFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        int counter=0;
        
        String fieldsstring="";
        for(int i=0;i<fields.size();i++)
        {
            fieldsstring+="\n            "+(i==0?"":",")+"\""+fields.get(i) +"\"";
        }
        
        String fieldtypesstring="";
        for(int i=0;i<fieldtypes.size();i++)
        {
            fieldtypesstring+="\n            "+(i==0?"":",")+"\""+fieldtypes.get(i) +"\"";
        }
        
        String idfield=fields.get(0);
        String idfieldtype=fieldtypes.get(0);
        String idfieldtypestringifier=stringifier(fieldtypes.get(0));
        String iddatatype=datatypeFor(fieldtypes.get(0));
        
        String gettersandsetters="";
        String datatype="";
        String field="";
        String fieldtype="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            datatype=datatypeFor(fieldtypes.get(i));
            gettersandsetters+=
"\n    public "+datatype+" get"+toCamelCase(field)+"() {"
+"\n            return "+field+";"
+"\n    }"
+"\n"
+"\n    public void set"+toCamelCase(field)+"("+datatype+" "+field+") {"
+"\n            this."+field+" = "+field+";"
+"\n    }"
+"\n";
        }
        
        String vardefinitions="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            datatype=datatypeFor(fieldtypes.get(i));
            vardefinitions+=
"\n    public "+datatype+" "+field+";";
        }
                        
//        String constructorfields="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            constructorfields+=
//"\n            "+field+"=rs."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\");";
//        }
 
        String implodevaluesstring="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            fieldtype=fieldtypes.get(i);
            implodevaluesstring+=
"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
        }
        
        String savestring="";
        if(iddatatype.contentEquals("String"))
            savestring="\n            if("+idfield+"==null || "+idfield+".isEmpty() )";
        else
            savestring="\n            if("+idfield+"==null || "+idfield+"==0)";
        
        String gettersstring="";
        for(int i=1;i<fields.size();i++)
        {
            field=fields.get(i);
            fieldtype=fieldtypes.get(i);
            datatype=datatypeFor(fieldtype);
            gettersstring+=
"\n    public static [tableCaps] getBy"+toCamelCase(field)+"("+datatype+" "+field+")"
+"\n    {"
+"\n            ArrayList<[tableCaps]> items=select(\" where "+field+" = '\"+"+field+stringifier(fieldtype) +"+\"'\");"
+"\n            for([tableCaps] item:items)return item;"
+"\n            return null;"
+"\n    }	"
+"\n    public static ArrayList<[tableCaps]> selectBy"+toCamelCase(field)+"("+datatype +" "+field+")"
+"\n    {"
+"\n            return select(\" where "+field+" = '\"+"+field+stringifier(fieldtype) +"+\"'\");"
+"\n    }	";
        }
        
        String lastinsertidfunctionstring=
"\n	public static Integer getLastInsertId() {"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance().getWritableDatabase();"
+"\n		Cursor cursor = db.rawQuery(\"SELECT last_insert_rowid() FROM \"+tablename+\" \", null);"
+"\n		cursor.moveToFirst();"
+"\n		Integer result=cursor.getInt(0);"
+"\n		cursor.close();"
+"\n		db.close();"
+"\n		return result;"
+"\n	}"
;       //if string id, don't add lastinsertid()
        if(iddatatype.contentEquals("String"))
            lastinsertidfunctionstring="";
        
        String insertfunction=
"\n	public static Integer insert([tableCaps] item)"
+"\n	{"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance().getWritableDatabase();"
+"\n		"
+"\n		if(fieldtypes[0].contains(\"int\"))"
+"\n		{"
+"\n			db.execSQL(\"INSERT INTO \"+tablename+\" (\"+implodeFields(false)+\")VALUES (\" "
+"\n					+implodeValues(item, false)"
+"\n					+\");\");"
+"\n		}"
+"\n		else"
+"\n		if(fieldtypes[0].contains(\"varchar\"))"
+"\n		{"
+"\n			db.execSQL(\"INSERT INTO \"+tablename+\" (\"+implodeFields(true)+\")VALUES (\" "
+"\n					+implodeValues(item, true)"
+"\n					+\");\");"
+"\n		}"
+"\n"
+"\n            //fetch last insert id"
+"\n		Cursor cursor = db.rawQuery(\"SELECT last_insert_rowid() FROM \"+tablename+\" \", null);"
+"\n		cursor.moveToFirst();"
+"\n		Integer result=cursor.getInt(0);"
+"\n		cursor.close();"
+"\n"
+"\n		db.close();"
+"\n		return result;"
+"\n	}";
        if(iddatatype.contentEquals("String"))
            insertfunction=
"\n	public static void insert([tableCaps] item)"
+"\n	{"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance().getWritableDatabase();"
+"\n		"
+"\n		if(fieldtypes[0].contains(\"int\"))"
+"\n		{"
+"\n			db.execSQL(\"INSERT INTO \"+tablename+\" (\"+implodeFields(false)+\")VALUES (\" "
+"\n					+implodeValues(item, false)"
+"\n					+\");\");"
+"\n		}"
+"\n		else"
+"\n		if(fieldtypes[0].contains(\"varchar\"))"
+"\n		{"
+"\n			db.execSQL(\"INSERT INTO \"+tablename+\" (\"+implodeFields(true)+\")VALUES (\" "
+"\n					+implodeValues(item, true)"
+"\n					+\");\");"
+"\n		}"
+"\n"
+"\n		db.close();"
+"\n	}";
        
        String output="package models.base;"
+"\n"
+"\nimport java.util.ArrayList;"
+"\nimport java.util.Date;"
+"\n"
+"\nimport models.[tableCaps];"
+"\nimport utils.MyDatabaseHelper;"
+"\nimport android.database.Cursor;"
+"\nimport android.database.sqlite.SQLiteDatabase;"
+"\n"
+"\npublic class Base[tableCapsPlural] {"
+"\n    //------------FIELDS-----------"
+"\n    public static final String tablename=[tableCaps].tablename;"
+"\n    public static String[] fields=[tableCaps].fields;"
+"\n    public static String[] fieldtypes=[tableCaps].fieldtypes;"
+"\n    //-----------------------"
+"\n    //-------------------------TABLE FUNCTIONS---------------------"
+"\n"
+"\n    //-----------getter functions----------"
+"\n    public static [tableCaps] getBy"+toCamelCase(idfield)+"("+iddatatype+" "+idfield+") {"
+"\n            ArrayList<[tableCaps]> map=select(\" where "+idfield+" = '\"+"+idfield+idfieldtypestringifier+"+\"'\");"
+"\n            for([tableCaps] item:map)return item;"
+"\n            return null;"
+"\n    }"
+gettersstring
+"\n    //-----------database functions--------------"
+"\n"
+"\n	public static void delete([tableCaps] item)"
+"\n	{"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance().getWritableDatabase();"
+"\n		db.execSQL(\"delete from \"+tablename+\" where id = '\"+item.getId()+\"';\");"
+"\n		db.close();"
+"\n	}"
+"\n	public static void delete(Integer id)"
+"\n	{"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance().getWritableDatabase();"
+"\n		db.execSQL(\"delete from \"+tablename+\" where id = '\"+id+\"';\");"
+"\n		db.close();"
+"\n	}"
+insertfunction
+"\n	public static void update([tableCaps] item)"
+"\n	{"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance().getWritableDatabase();"
+"\n"
+"\n		db.execSQL("
+"\n		\"update \"+tablename+\" set \"+implodeFieldsWithValues(item,false)+\" where id = '\"+item.getId()"
+"\n		+\"';\");"
+"\n		db.close();"
+"\n	}"
+"\n	public static ArrayList<[tableCaps]> select(String criteria) {"
+"\n		ArrayList<[tableCaps]> items = new ArrayList<[tableCaps]>();"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance()"
+"\n				.getWritableDatabase();"
+"\n"
+"\n		Cursor cursor = db.rawQuery(\"SELECT * FROM \"+tablename+\" \"+criteria, null);"
//+"\n		items = new ArrayList<[tableCaps]>();"
+"\n		while (cursor.moveToNext()) {"
+"\n			items.add(new [tableCaps](cursor));"
+"\n		}"
+"\n		cursor.close();"
+"\n		db.close();"
+"\n		return items;"
+"\n	}"
+"\n	public static Integer count(String criteria) {"
+"\n		SQLiteDatabase db = MyDatabaseHelper.getInstance().getWritableDatabase();"
+"\n		Cursor cursor = db.rawQuery(\"SELECT count(*) FROM \"+tablename+\" \"+criteria, null);"
+"\n		cursor.moveToFirst();"
+"\n		Integer result=cursor.getInt(0);"
+"\n		cursor.close();"
+"\n		db.close();"
+"\n		return result;"
+"\n	}"                
+lastinsertidfunctionstring
+"\n"
+"\n    //-----------database helper functions--------------"
+"\n    public static String implodeValues([tableCaps] item,boolean withId)"
+"\n    {"
+"\n            ArrayList<String> values=item.implodeFieldValuesHelper(withId);"
+"\n            String output=\"\";"
+"\n            for(String value:values)"
+"\n            {"
+"\n                    if(!output.isEmpty())"
+"\n                            output+=\",\";"
+"\n                    output+=(value!=null?\"'\"+value+\"'\":\"null\");"
+"\n            }"
+"\n            return output;"
+"\n    }"
+"\n    public static String implodeFields(boolean withId)"
+"\n    {"
+"\n            String output=\"\";"
+"\n            for(String field:fields)"
+"\n            {"
+"\n                    if(!withId && field.contentEquals(\""+idfield+"\"))continue;"
+"\n                    if(!output.isEmpty())"
+"\n                            output+=\",\";"
+"\n                    output+=field;"
+"\n            }"
+"\n            return output;"
+"\n    }"
+"\n    public static String implodeFieldsWithValues([tableCaps] item,boolean withId)"
+"\n    {"
+"\n            ArrayList<String> values=item.implodeFieldValuesHelper(true);//get entire list of values; whether the id is included will be dealt with later."
+"\n"
+"\n            if(values.size()!=fields.length)"
+"\n            {"
+"\n                    System.err.println(\"[tableCapsPlural]:implodeFieldsWithValues(): ERROR: values length does not match fields length\");"
+"\n            }"
+"\n"
+"\n            String output=\"\";"
+"\n            for(int i=0;i<fields.length;i++)"
+"\n            {"
+"\n                    if(!withId && fields[i].contentEquals(\""+idfield+"\"))continue;"
+"\n                    if(!output.isEmpty())"
+"\n                            output+=\",\";"
+"\n                    output+=fields[i]+\"=\"+(values.get(i)!=null?\"'\"+values.get(i)+\"'\":\"null\");"
+"\n            }"
+"\n            return output;"
+"\n    }	"
+"\n    public static String implodeFieldsWithTypes()"
+"\n    {"
+"\n            String output=\"\";"
+"\n            for(int i=0;i<fields.length;i++)"
+"\n            {"
+"\n                    if(fields[i].contentEquals(fields[0]))//fields[0] being the primary key"
+"\n                            output+=fields[i]+\" \"+fieldtypes[i]+\" PRIMARY KEY\";"
+"\n                    else"
+"\n                            output+=\",\"+fields[i]+\" \"+fieldtypes[i];"
+"\n            }"
+"\n            return output;"
+"\n    }	"
+"\n    public static String createTable()"
+"\n    {"
+"\n            return \"CREATE TABLE IF NOT EXISTS \"+tablename+\" (\"+implodeFieldsWithTypes()+\" );\";"
+"\n    }"
+"\n    public static String deleteTable()"
+"\n    {"
+"\n            return \"DROP TABLE IF EXISTS \"+tablename;"
+"\n    }"
+"\n}"
+"\n";
//        String tablecaps=toCamelCase(table);
//        String tablecapsplural=toCamelCase(table+"s");
//        String tableplural=table+"s";
//        output=output.replace("[tableCaps]", tablecaps);
//        output=output.replace("[table]", table);
//        output=output.replace("[tableCapsPlural]", tablecapsplural);
//        output=output.replace("[tablePlural]", tableplural);
        String tableplural=table;
        String tablesingular=singularize(table);
        String tablecapssingular=toCamelCase(tablesingular);
        String tablecapsplural=toCamelCase(tableplural);
        output=output.replace("[tableCaps]", tablecapssingular);
        output=output.replace("[table]", table);
        output=output.replace("[tableCapsPlural]", tablecapsplural);
        output=output.replace("[tablePlural]", tableplural);

        return output;
    }
    public static void createModelFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        //write file to current directory
        File outputdir=new File("./src/models");
        outputdir.mkdir();
        FileWriter.write(outputdir.getPath()+"/"+toCamelCase(singularize(table))+".java", createModelFileContents(table,fields,fieldtypes));
    }
    public static void createBaseModelFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        //write file to current directory
        File outputdir=new File("./src/models/base");
        outputdir.mkdir();
        FileWriter.write(outputdir.getPath()+"/Base"+toCamelCase(singularize(table))+".java", createBaseModelFileContents(table,fields,fieldtypes));
    }
    public static String createBaseModelFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        int counter=0;
        
        String fieldsstring="";
        for(int i=0;i<fields.size();i++)
        {
            fieldsstring+="\n            "+(i==0?"":",")+"\""+fields.get(i) +"\"";
        }
        
        String fieldtypesstring="";
        for(int i=0;i<fieldtypes.size();i++)
        {
            fieldtypesstring+="\n            "+(i==0?"":",")+"\""+fieldtypes.get(i) +"\"";
        }
        
        String idfield=fields.get(0);
        String idfieldtype=fieldtypes.get(0);
        String idfieldtypestringifier=stringifier(fieldtypes.get(0));
        String iddatatype=datatypeFor(fieldtypes.get(0));
        
        String gettersandsetters="";
        String datatype="";
        String field="";
        String fieldtype="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            datatype=datatypeFor(fieldtypes.get(i));
            gettersandsetters+=
"\n    public "+datatype+" get"+toCamelCase(field)+"() {"
+"\n            return "+field+";"
+"\n    }"
+"\n"
+"\n    public void set"+toCamelCase(field)+"("+datatype+" "+field+") {"
+"\n            this."+field+" = "+field+";"
+"\n    }"
+"\n";
        }
        
        String vardefinitions="";
        String initialvalue="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            datatype=datatypeFor(fieldtypes.get(i));
            if(datatype.contentEquals("Integer"))
                initialvalue="0";
            else if(datatype.contentEquals("Long"))
                initialvalue="0l";
            else if(datatype.contentEquals("String"))
                initialvalue="\"\"";
            else if(datatype.contentEquals("Boolean"))
                initialvalue="false";
            else 
                initialvalue="\"\"";
            vardefinitions+=
"\n    public "+datatype+" "+field+"="+initialvalue+";";
        }
                        
        String constructorfields="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            constructorfields+=
                    //id=c.getString(c.getColumnIndex("id"));
"\n        "+field+"="+rsGetterFor(fieldtypes.get(i), field);
        }
 
        String implodevaluesstring="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            fieldtype=fieldtypes.get(i);
            implodevaluesstring+=
"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
        }
        
        String savestring="";
        if(iddatatype.contentEquals("String"))
        {
            savestring="\n            if("+idfield+"==null || "+idfield+".isEmpty() )";
            savestring+="\n                    [tableCapsPlural].insert(([tableCaps])this);";
        }
        else
        {
            savestring="\n            if("+idfield+"==null || "+idfield+"==0)";
            savestring+="\n                    "+idfield+"=Integer.valueOf([tableCapsPlural].insert(([tableCaps])this));";
        }
        
        
        String output="package models.base;"
+"\n"
+"\nimport java.util.ArrayList;"
+"\nimport java.util.Date;"
+"\n"
+"\nimport utils.StandardDateHelper;"
+"\nimport models.[tableCapsPlural];"
+"\nimport models.[tableCaps];"
+"\nimport android.database.Cursor;"
+"\n"
+"\npublic class Base[tableCaps] {"
+"\n    //------------FIELDS-----------"
+"\n    public static final String tablename=\"[table]\";"
+"\n    //field names"
+"\n    public static String[] fields={"
+fieldsstring
+"\n            };"
+"\n    //field types"
+"\n    public static String[] fieldtypes={"
+fieldtypesstring
+"\n            };"
+"\n    //-----------------------"
+"\n"
+vardefinitions
+"\n"
+"\n    public Base[tableCaps]() {"
+"\n    }"
+"\n    public Base[tableCaps](Cursor c) {"
+constructorfields
+"\n    }"
+"\n"
+"\n//	public String getUuid()"
+"\n//	{"
+"\n//		return id.toString()+\"-\";"
+"\n//	}"
+"\n"
+gettersandsetters
+"\n"
+"\n    //database functions"
+"\n    public ArrayList<String> implodeFieldValuesHelper(boolean withId)"
+"\n    {"
+"\n            ArrayList<String> values=new ArrayList<String>(); "
+"\n"
+"\n            //add values for each field here"
+implodevaluesstring
+"\n"
+"\n            return values;"
+"\n    }"
+"\n    public void delete()"
+"\n    {"
+"\n            [tableCapsPlural].delete(([tableCaps])this);"
+"\n    }"
+"\n    public void save()"
+"\n    {"
+savestring
+"\n            else"
+"\n                    [tableCapsPlural].update(([tableCaps])this);"
+"\n    }"
+"\n    @Override"
+"\n    public String toString()"
+"\n    {"
+"\n            return "+idfield+idfieldtypestringifier+";"
+"\n    }"
+"\n}"
+"\n";
//        String tablecaps=toCamelCase(table);
//        String tablecapsplural=toCamelCase(table+"s");
//        String tableplural=table+"s";
//        output=output.replace("[tableCaps]", tablecaps);
//        output=output.replace("[table]", table);
//        output=output.replace("[tableCapsPlural]", tablecapsplural);
//        output=output.replace("[tablePlural]", tableplural);
        String tableplural=table;
        String tablesingular=singularize(table);
        String tablecapssingular=toCamelCase(tablesingular);
        String tablecapsplural=toCamelCase(tableplural);
        output=output.replace("[tableCaps]", tablecapssingular);
        output=output.replace("[table]", table);
        output=output.replace("[tableCapsPlural]", tablecapsplural);
        output=output.replace("[tablePlural]", tableplural);
        
        return output;
    }
    private static String singularize(String table)
    {
        String singular=table;
        if(table.substring(table.length()-3, table.length()).contentEquals("ies"))
        {
            singular=table.substring(0,table.length()-3)+"y";
        }
        else if(table.substring(table.length()-1, table.length()).contentEquals("s"))
        {
            singular=table.substring(0,table.length()-1);
        }
        return singular;
    }
    public static String toCamelCase(String string)
    {
        String[] segments=string.split("_");
        String output="";
        for(String s:segments)
            output+=capitalize(s);
        return output;
    }
    public static String capitalize(String s)
    {
        return s.substring(0, 1).toUpperCase()+s.substring(1);
    }
    public static String datatypeFor(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        //System.out.println(type);
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "Integer";
        else if(type.contentEquals("varchar()"))
            return "String";
        else if(type.contentEquals("char()"))
            return "String";
        else if(type.contentEquals("text"))
            return "String";
        else if(type.contentEquals("tinytext"))
            return "String";
        else if(type.contentEquals("date"))
            return "Date";
        else if(type.contains("bigint"))
            return "Long";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "Integer";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "BigDecimal";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "Float";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "Double";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "Boolean";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "Timestamp";
        else if(type.contains("enum"))
            return "String";
        else 
            return "";
/*
<option value="INT" selected="selected">INT</option>
<option value="VARCHAR">VARCHAR</option>
<option value="TEXT">TEXT</option>
<option value="DATE">DATE</option>
<optgroup label="NUMERIC"><option value="TINYINT">TINYINT</option>
<option value="SMALLINT">SMALLINT</option>
<option value="MEDIUMINT">MEDIUMINT</option>
<option value="INT" selected="selected">INT</option>
<option value="BIGINT">BIGINT</option>
<option value="-">-</option>
<option value="DECIMAL">DECIMAL</option>
<option value="FLOAT">FLOAT</option>
<option value="DOUBLE">DOUBLE</option>
<option value="REAL">REAL</option>
<option value="-">-</option>
<option value="BIT">BIT</option>
<option value="BOOLEAN">BOOLEAN</option>
<option value="SERIAL">SERIAL</option>
</optgroup><optgroup label="DATE and TIME"><option value="DATE">DATE</option>
<option value="DATETIME">DATETIME</option>
<option value="TIMESTAMP">TIMESTAMP</option>
<option value="TIME">TIME</option>
<option value="YEAR">YEAR</option>
</optgroup><optgroup label="STRING"><option value="CHAR">CHAR</option>
<option value="VARCHAR">VARCHAR</option>
<option value="-">-</option>
<option value="TINYTEXT">TINYTEXT</option>
<option value="TEXT">TEXT</option>
<option value="MEDIUMTEXT">MEDIUMTEXT</option>
<option value="LONGTEXT">LONGTEXT</option>
<option value="-">-</option>
<option value="BINARY">BINARY</option>
<option value="VARBINARY">VARBINARY</option>
<option value="-">-</option>
<option value="TINYBLOB">TINYBLOB</option>
<option value="MEDIUMBLOB">MEDIUMBLOB</option>
<option value="BLOB">BLOB</option>
<option value="LONGBLOB">LONGBLOB</option>
<option value="-">-</option>
<option value="ENUM">ENUM</option>
<option value="SET">SET</option>
</optgroup><optgroup label="SPATIAL"><option value="GEOMETRY">GEOMETRY</option>
<option value="POINT">POINT</option>
<option value="LINESTRING">LINESTRING</option>
<option value="POLYGON">POLYGON</option>
<option value="MULTIPOINT">MULTIPOINT</option>
<option value="MULTILINESTRING">MULTILINESTRING</option>
<option value="MULTIPOLYGON">MULTIPOLYGON</option>
<option value="GEOMETRYCOLLECTION">GEOMETRYCOLLECTION</option>
</optgroup>    
 */        
    }
    public static String rsGetterFor(String type, String field)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "c.getInt"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contains("varchar()"))
            return "c.getString"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("char()"))
            return "c.getString"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("text"))
            return "c.getString"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("tinytext"))
            return "c.getString"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("date"))
            return "StandardDateHelper.toDate(c.getString"+"(c.getColumnIndex(\""+field+"\")));";
        else if(type.contains("bigint"))
            return "c.getLong"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "c.getInt"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "c.getBigDecimal"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "c.getFloat"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "c.getDouble"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "c.getBoolean"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "c.getTimestamp"+"(c.getColumnIndex(\""+field+"\"));";
        else if(type.contains("enum"))
            return "c.getString"+"(c.getColumnIndex(\""+field+"\"));";
        else 
            return "";  
    }    
    public static String stringifier(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return ".toString()";
        else if(type.contains("varchar()"))
            return "";
        else if(type.contentEquals("char()"))
            return "";
        else if(type.contentEquals("text"))
            return "";
        else if(type.contentEquals("tinytext"))
            return "";
        else if(type.contentEquals("date"))
            return ".toString()";
        else if(type.contains("bigint"))
            return ".toString()";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return ".toString()";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return ".toString()";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return ".toString()";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return ".toString()";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return ".toString()";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return ".toString()";
        else if(type.contains("enum"))
            return "";
        else 
            return "";  
    }       
    public static String stringifiedWithNull(String field,String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contains("varchar()"))
            return field;
        else if(type.contentEquals("char()"))
            return field;
        else if(type.contentEquals("text"))
            return field;
        else if(type.contentEquals("tinytext"))
            return field;
        else if(type.contentEquals("date"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contains("bigint"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return field+"!=null?"+field+".toString():null";
        else if(type.contains("enum"))
            return field;
        else 
            return field;  
    }    
     
}
